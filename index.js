const connectToDB = require('./db');
const express = require('express')
// const cors = require("cors");
const fs = require('fs');
const https = require('https');
connectToDB();
const app = express()
const port = 443
const privateKey  = fs.readFileSync('ssl_cert/server.key', 'utf8');
const certificate = fs.readFileSync('ssl_cert/server.crt', 'utf8');
const credentials = {key: privateKey, cert: certificate};
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "https://app.inotebook.tech");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, auth-token");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  next();
})
app.use(express.json())
// Available Routes
app.use('/api/auth', require('./routes/auth'))
app.use('/api/notes', require('./routes/notes'))
app.get("/api/test", (req, res) => {
  res.send("This is home page.");
});
// Start the server

//app.listen(port,"0.0.0.0", () => {
//    console.log(`Example app listening on port ${port}`)
//})
const server = https.createServer(credentials, app);

server.listen(port, () => {
  console.log("server starting on port : " + port)
});
